package br.ufc.quixada.es.model;

public class Corrida {
	
	private String partida;
	private String destino;
	private float precoKm;
	private float precoCorrida;
	
	public Corrida(){
		
	}
	
	public Corrida(String partida, String destino, float precoKm, float precoCorrida){
		this.partida = partida;
		this.destino = destino;
		this.precoKm = precoKm;
		this.precoCorrida = precoCorrida;
	}
	
	public String getPartida() {
		
		return this.partida;
	}
	
	public String getDestino() {
		
		return this.destino;
	}

	public float getPrecoKm() {
	
		return this.precoKm;
	}
	
	public float getPrecoCorrida() {
		
		return this.precoCorrida;
	}
	
	public void setPartida(String partida) {
	
		this.partida = partida;
	}
	
	public void setDestino(String destino) {
		
		this.destino = destino;
	}
	
	public void setPrecoKm(float precoKm) {
		
		this.precoKm = precoKm;
	}
	
	public void setPrecoCorrida(float precoCorrida) {
		
		this.precoCorrida = precoCorrida;
	}
	
	public float calcularValorCorrida(int kmPercorridos) {
		
		float valorCorrida = 0;
		
		valorCorrida = (kmPercorridos * precoKm) + 5;
		
		return this.precoCorrida = valorCorrida;
	}
	
	public String toString(){
		
		return "Partida = " + partida + "\n" + "Destino = " + destino + "\n" + "Pre�o do KM = " + precoKm + "\n" + "Pre�o da Corrida = " + precoCorrida + "\n";
	}
}
