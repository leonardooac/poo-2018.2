package br.ufc.quixada.es.model;

public class Cliente {
	
	private String nome;
	private String cpf;
	
	
	public Cliente(){
		
	}
	
	public Cliente(String nome, String cpf){
		this.nome = nome;
		this.cpf = cpf;
	}
	
	public String getNome() {
		
		return this.nome;
	}
	
	public String getCpf() {
		
		return this.cpf;
	}
	
	public void setNome(String nome) {
		
		this.nome = nome;
	}
	
	public void setCpf(String cpf) {
		
		this.cpf = cpf;
	}
	
	public void darNotaAoMotorista(Motorista mot, float nota) {
		mot.setNota(nota);
	}
	
	public String toString(){
		
		return "Nome = " + nome + "\n" + "CPF = " + cpf + "\n";
	}
}
