package br.ufc.quixada.es.model;

public class Motorista {
	
	private String nome;
	private String cnh;
	private String placa;
	private float nota;
	
	
	public Motorista(){
		
	}
	
	public Motorista (String nome, String cnh, String placa, float nota){
		this.nome = nome;
		this.cnh = cnh;
		this.placa = placa;
		this.nota = nota;
	}
	
	public String getNome() {
		
		return this.nome;
	}
	
	public String getCnh() {
		
		return this.cnh;
	}

	public String getPlaca() {
	
		return this.placa;
	}
	
	public float getNota() {
		
		return this.nota;
	}
	
	public void setNome(String nome) {
	
		this.nome = nome;
	}
	
	public void setCnh(String cnh) {
		
		this.cnh = cnh;
	}
	
	public void setPlaca(String placa) {
		
		this.placa = placa;
	}
	
	public void setNota(float nota) {
		
		this.nota = nota;
	}
	
	public void realizarCorrida(Cliente c, Corrida cor) {
		System.out.println(c.getNome());
		System.out.println(getNome());
		System.out.println(getNota());
		System.out.println(cor.getPartida());
		System.out.println(cor.getDestino());
	}
	
	public String toString(){
		
		return "Nome = " + nome + "\n" + "CNH = " + cnh + "\n" + "Placa = " + placa + "\n" + "Nota = " + nota + "\n";
	}
}
