package br.ufc.quixada.es.exec;
import br.ufc.quixada.es.model.Cliente;
import br.ufc.quixada.es.model.Corrida;
import br.ufc.quixada.es.model.Motorista;

public class Principal {

	public static void main(String[] args) {
	///////////////////////////////
	Motorista m1;
	Motorista m2;
	Motorista m3;
	
	m1 = new Motorista("Chico", "888888","ABC01232", 0);
	m2 = new Motorista("Jos�", "777777", "DFE0213", 0);
	m3 = new Motorista("Paulo", "555555", "FAS4353",0);
	//////////////////////////////
	Corrida cor1;
	Corrida cor2;
	Corrida cor3;

	cor1 = new Corrida("A","B", 10.0f, 0);
	cor2 = new Corrida("C","D", 20.0f, 0);
	cor3 = new Corrida("E","F", 30.0f, 0);
	/////////////////////////////
	Cliente c1;
	Cliente c2;
	Cliente c3;
	
	c1 = new Cliente("Joao","111.111.111-22");
	c2 = new Cliente("Carlos","000.000.000-33");
	c3 = new Cliente("Andre","222.222.222-99");
	/////////////////////////////
	c1.darNotaAoMotorista(m1, 7.2f);
	c2.darNotaAoMotorista(m2, 8.3f);
	c3.darNotaAoMotorista(m3, 9.5f);
	
	m1.realizarCorrida(c1, cor1 );
	m2.realizarCorrida(c2, cor2);
	m3.realizarCorrida(c3, cor3);
	
	cor1.calcularValorCorrida(10);
	cor2.calcularValorCorrida(20);
	cor3.calcularValorCorrida(30);
	
	System.out.println(cor1.toString());
	System.out.println(cor2.toString());
	System.out.println(cor3.toString());
	}

}
