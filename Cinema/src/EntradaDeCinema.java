
public class EntradaDeCinema {
	
	private String tituloDoFilme;
	private String horario;
	private String sala;
	private String poltrona;
	private double valorOriginal;
	private boolean disponivel;
	
	EntradaDeCinema (String tituloDoFilme, String horario, String sala, String poltrona, double valorOriginal, boolean disponivel){
		this.tituloDoFilme = tituloDoFilme;
		this.horario = horario;
		this.sala = sala;
		this.poltrona = poltrona;
		this.valorOriginal = valorOriginal;
		this.disponivel = disponivel;
	}
	
	public String getTituloDoFilme() {
		
		return this.tituloDoFilme;
	}
	
	public String getHorario() {
		
		return this.horario;
	}
	
	public String getSala() {
		
		return this.sala;
	}
	
	public String getPoltrona() {
		
		return this.poltrona;
	}
	
	public double getValorOriginal() {
		
		return this.valorOriginal;
	}
	
	public double calculaValorComDesconto(int ano, int numeroCarteira) {
		
		//double valorDesconto = 0; 
		
		if (ano>=2007) {
			valorOriginal = valorOriginal * 0.5;
			
			return valorOriginal;
		}else if(ano<=2006 && ano>=2003){
			valorOriginal = valorOriginal *0.6;
			
			return valorOriginal;
		}else if(ano<=2002 && ano>=1998 ) {
			valorOriginal = valorOriginal *0.7;
			
			return valorOriginal;
		}else if(ano<=1997) {
			valorOriginal = valorOriginal *0.8;
			
			return valorOriginal;
		}else {
			return this.valorOriginal;
		}
		
	}
	
	public boolean realizarVenda() {
		
		return this.disponivel = false;
	}
	
	public String toString(){
		
		return "T�tulo do Filme = " + tituloDoFilme + "\n" + "Hor�rio = " + horario + "\n" + "Sala = " + sala + "\n" + "Poltrona = " + poltrona + "\n" + "Valor = " + valorOriginal +"\n" + "Disponibilidade = " + disponivel + "\n";
	}
		
	
	public static void main(String[] args) {
		EntradaDeCinema entrada1;
		entrada1 = new EntradaDeCinema("Bastardos Ingl�rios" , "15:00", "1","1", 8.50, true);
		entrada1.realizarVenda();
		entrada1.calculaValorComDesconto(2008, 0);
		System.out.println(entrada1.toString());
		
		
		EntradaDeCinema entrada2;
		entrada2 = new EntradaDeCinema("Tropa de Elite" , "18:00", "3","2", 12.00, true);
		entrada2.realizarVenda();
		entrada2.calculaValorComDesconto(1997, 0);
		System.out.println(entrada2.toString());
		
		EntradaDeCinema entrada3;
		entrada3 = new EntradaDeCinema("A Freira" , "21:00", "5","3", 15.00, true);
		entrada3.realizarVenda();
		entrada3.calculaValorComDesconto(2002, 0);
		System.out.println(entrada3.toString());
	}

}
